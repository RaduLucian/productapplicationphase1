#include "Product.h"

#include <iostream>
#include <regex>
#include <vector>
#include <fstream>

bool isNonPerishableProduct(const std::string& type)
{
	return std::regex_match(type, std::regex(R"(\d\d\d\d-\d\d-\d\d)"));
}

bool priceComparator(const Product* left, const Product* right)
{
	return left->getRawPrice() < right->getRawPrice();
}

bool nameComparator(const Product* left, const Product* right)
{
	return left->getName() < right->getName();
}

int main()
{
	std::vector<Product*> products;

	uint16_t id;
	std::string name;
	float rawPrice;
	uint16_t vat;
	std::string type;

	for (std::ifstream in("product.prodb"); !in.eof();)
	{
		in >> id >> name >> rawPrice >> vat >> type;

		products.push_back(new Product(id, name, rawPrice, vat, type));
	}

	std::sort(products.begin(), products.end(), priceComparator);

	for (auto& product : products)
	{
		if (isNonPerishableProduct(product->getType()))
		{
			std::cout << product->getId() << " "
				<< product->getName() << " "
				<< product->printPrice()
				<< std::endl;
		}
	}
}