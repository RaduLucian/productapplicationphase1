# Products Application

You have been appointed the manager of a new general store. As you know some programming, you will be creating a software that manages the inventory logic.

## Requirements

### Phase 1

1. Read the products from the input file product.prodb into a vector. Use a single class to represent a product. Watch out for member variable types, memory is precious! No setters allowed, we want a product object to be immutable!
1. Print only the NonperishableProducts on the screen, with the price calculated based on VAT.
1. Provide functionality to sort by name or by price.